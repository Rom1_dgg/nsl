# Instructions for the notebooks

I use notebooks for development, you will find instructions about them here. 

Libraries : `Polars`, `Seaborn`, `sklearn`, `wordcloud`, `TensorFlow`, `LDA`

## Notebook `scrapping_trip_allpages.ipynb`

I scrap all the reviews and save them in a `.txt` file. 

Libraries : `requests`, `beautifulsoup4`

## Notebook `cleaning_trip_allpages.ipynb`

I clean the html data stored. 

Libraries : `Pandas`

## Notebook `aed_avistrip.ipynb`

I conduct some exploratory data analysis. 

Libraries : `Pandas`, `Seaborn` 

## Notebook `nlp_classification.ipynb`

I use NLP algorithm to analyze text data.

Libraries : `LDA`, `TF-IDF`, `CountVectorizer`

## Notebook `keybert_extraction.ipynb`

I use KeyBERT to extract key words. 

