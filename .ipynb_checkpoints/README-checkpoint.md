# nsl

In this project called `nsl` I analyze TripAdvisor reviews of the Natural Surf Lodge. I do it in using `Python`. 

I use `Python` to apply Natural Language Processing, Machine Learning and Deep Learning techniques and I visualize my data .

Technologies : `Python`, `Polars`

⚠️ This is a personal project. 

## Dependencies/prerequisites

- Python 3.12.4
- Main dataset available from scrapping in `data/raw/`
    - It gathers several Trip Advisor reviews
    - Clean dataset is in `data/preprocessed/`
    - Each line of this file includes Trip Advisor review

### Install a virtual environment (recommended)

```bash
pip install virtualenv
python3 -m venv .venv
source .venv/bin/activate
```

### Installing dependencies 

```bash
pip install -r requirements.txt
```

## Project structure 

- `data/` contains data from public datasets and any external data source
- `intermediate/`contains processed data ready for training
- `notebooks/` contains notebooks for exploration and prototyping
- `output/` contains generated figures, plots and reports
- `R/` contains R scripts for `{targets}` pipeline
- `python` contains Python scripts `.py` for production
- `SQL` contains SQL queries 
- `requirements.txt` contains dependencies
- `README.md` gives the project overview and instructions
- `.gitignore` contains files and folders to be ignored








